# Make Dumb Inverter AC Smart
With the combination of Home Assistant, Node-RED and Broadlink RM device, I've made it possible to add my IR-only A/C to Home Assistant with even more functionality than the A/C has.

![ha.png](ha.png)

## Features
### Auto mode
- You can make A/C run according to **the room temperature sensor** you have on HA instead of relying on the sensor on the A/C which can be unreliable. I'm **NOT** doing it simply by turning **on and off**. Because, it defeats the purpose of **inverter** and when turned off after cooling operation without the fan blowing your A/C it is more likely to grow **bacteria**. I'm using a combination of changing set temperature of A/C, fan-only and dry modes to maintain the desired temperature.
- Ability to set temperatures with a step of **0.5 degrees** even if your A/C doesn't support it.
- **Energy saving mode** tricks the A/C to run the compressor to run at lower speeds which is more efficient. 
- **Low humidity prevention** prevents getting air too dry by preventing low fan speeds when the compressor is working if humidity is starting to get too low because higher fan speeds reduce the rate of moisture collection.

### Self-clean 
To prevent bacteria growth, after the cooling operation, when the A/C is turned off via Home Assistant, A/C will work fan-only for a few hours and then turn off. This feature will work even if your A/C doesn't support it.

### Other supported functionality
- **Operating modes:** Cool, Heat, Dry, Fan-only
- **Fan modes:** Auto, Low, Medium, High
	- If **low, medium or high** fan modes are selected, the temperature setting will be sent to the A/C and the A/C will rely on its own sensor. 
	- If **auto** fan mode is selected, auto mode will be activated with the features above. A/C settings will be set 
- **Swing:** On or off



## Requirements
- A/C that sends all of the settings in one IR code. (You can tell if previous settings are applied when you change settings by blocking the IR emitter and then changing a different setting without blocking it). 
- Broadlink RM, universal remote
- An MQTT Broker
- Home Assistant
  - [Node-RED Companion](https://github.com/zachowj/hass-node-red)
- Node-RED
	- [node-red-contrib-home-assistant-websocket](https://github.com/zachowj/node-red-contrib-home-assistant-websocket)
	- [node-red-contrib-broadlink-control](https://github.com/mlfunston/node-red-contrib-broadlink-control)

## Instructions
### Defining IR Codes
If you're using a Vestel or SEG air conditioner, I got you covered, just copy the **SharedData** to your node-red folder. For different A/Cs, refer to the README.md file located in the **SharedData** directory.

### Home Assistant Configuration
- Copy the content of the `configuration.yaml` file in the repo to `configuration.yaml` of Home Assistant. 
- Make edits if needed. (maybe change the name)
- Restart Home Assistant.
- For self-clean functionality, create automation and edit as YAML, then copy the content of `self-clean-automation.yaml` 

### Node-RED Configuration
- Import the `flows.json` file.
- Set **A/C** node with the name of the A/C entity on Home Assistant
- Set **Room Temperature** node with the temperature sensor of the room with the A/C.
- Set **RM** node according to your Broadlink device.
![nodered.png](nodered.png)
## Contributing and help
Please use the **Issues** section for any questions and problems.
Depending on the interest, I can shape the repository to make it compatible with more devices and use cases. 

It might be far from final product for some use cases and devices. I created this config years ago but never had time to touch it ever since. I decided to publish it because maybe it will be useful at least the idea itself. My JS code is also kind of spaghetti :) Pull requests are very welcome.