# IR Codes
The files in this folder are for **Vestel** or **SEG** A/Cs. 
Please refer [this article](https://flows.nodered.org/node/node-red-contrib-broadlink-control) to extract codes from your own A/C using the names and functions stated on the table and copy the **SharedData** on your phone to **nodred** folder.

**Notice:** Be sure to turn off **beep** sound before starting. Some A/Cs may forget this setting after off mode, so don't forget to turn off the beep again.

| name  | function, fan speed, swing, temperature) |
|-------|------------------------------------------|
| off   | off                                      |
| hl18  | Heat, low, off, 18                       |
| hl19  | Heat, low, off, 19                       |
| hl20  | Heat, low, off, 20                       |
| hl21  | Heat, low, off, 21                       |
| hl22  | Heat, low, off, 22                       |
| hl23  | Heat, low, off, 23                       |
| hl24  | Heat, low, off, 24                       |
| hl25  | Heat, low, off, 25                       |
| hl26  | Heat, low, off, 26                       |
| cl23  | Cool, low, off, 23                       |
| cl24  | Cool, low, off, 24                       |
| cl25  | Cool, low, off, 25                       |
| cl26  | Cool, low, off, 26                       |
| cls23 | Cool, low, on, 23                        |
| cls24 | Cool, low, on, 24                        |
| cls25 | Cool, low, on, 25                        |
| cls26 | Cool, low, on, 26                        |
| cm23  | Cool, medium, off, 23                    |
| cm24  | Cool, medium, off, 24                    |
| cm25  | Cool, medium, off, 25                    |
| cm26  | Cool, medium, off, 26                    |
| cms23 | Cool, medium, on, 23                     |
| cms24 | Cool, medium, on, 24                     |
| cms25 | Cool, medium, on, 25                     |
| cms26 | Cool, medium, on, 26                     |
| ch23  | Cool, high, off, 23                      |
| ch24  | Cool, high, off, 24                      |
| ch25  | Cool, high, off, 25                      |
| ch26  | Cool, high, off, 26                      |
| chs23 | Cool, high, on, 23                       |
| chs24 | Cool, high, on, 24                       |
| chs25 | Cool, high, on, 25                       |
| chs26 | Cool, high, on, 26                       |
| flu   | Fan-only, low, off                       |
| fmm   | Fan-only, medium, off                    |
| fhm   | Fan-only, high, off                      |
| fls   | Fan-only, low, on                        |
| fms   | Fan-only, medium, on                     |
| fhs   | Fan-only, high, on                       |
| dry   | dry, 18                                  |

