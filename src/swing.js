let getMsg = msg.payload;
let ac = global.get('ac');

if (!ac) {
    ac = {
        power: "ON",
        mode: "fan_only",
        temperature: 24,
        fan: "medium",
        swing: getMsg
    }
} else {
    ac.swing = getMsg;
}

global.set('ac', ac);
return msg;
