let getMsg = msg.payload;
let ac = global.get('ac');

if (!ac) {
    ac = {
        power: "ON",
        mode: getMsg,
        temperature: 24,
        fan: "auto",
        swing: "off"
    }
} else {
    ac.mode = getMsg;
}

global.set('ac', ac);
return msg;
