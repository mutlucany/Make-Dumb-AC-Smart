let ac = global.get('ac');
let eco = global.get('eco');
let lastAc = global.get('lastAc');
const state = global.get('states');
const hum = parseFloat(state['sensor.mi_h_4c65a8df0da1'].payload)
if (ac.mode === "cool" && ac.fan === "auto") {
    if (msg.payload - .4 > ac.temperature) {
        if (ac.swing === "on") {
            if (eco === 'on') {
                msg.payload = "cms24";
            } else {
                msg.payload = "chs23";
            }
        } else {
            if (eco === 'on') {
                msg.payload = "dry";
            } else {
                msg.payload = "ch23";
            }
            
        }
    } else if (msg.payload <= ac.temperature) {
        if (ac.swing === "on") {
            msg.payload = "fms";
        } else {
            msg.payload = "flu";
        }
    } else if (msg.payload > ac.temperature) {
        if (ac.swing === "on") {
           if (eco === 'on') {
                msg.payload = "cms23";
            } else {
                msg.payload = "cms23";
            }
        } else {
            if (eco === 'on') {
                msg.payload = "dry";
            } else {
                msg.payload = "cm23";
            }
        }
    } else {
        if (ac.swing === "on") {
            msg.payload = "cms23";
        } else {
            if (eco === 'on') {
                msg.payload = "dry";
            } else {
                msg.payload = "cm23";
            }
        }
    }
} else if (ac.mode === "heat" && ac.fan === "auto") {
    if (msg.payload < ac.temperature - .5) {
        if (eco === 'on') {
            msg.payload = "hm26";
        } else {
            msg.payload = "hh26";
        }
        
    } else if (msg.payload - .1 > ac.temperature) {
        msg.payload = "hl18";
        if (ac.temperature === "18" || ac.temperature === "19") msg.payload = "off";
    } else if (msg.payload >= ac.temperature) {
        msg.payload = "hl26";
    } else {
        msg.payload = "hm26";
    }
}

if (msg.payload === 'dry' && hum < 45) {
    msg.payload = 'cm23';
};

if (msg && msg.payload !== lastAc) {
    global.set('lastAc', msg.payload);
    return msg;
} else if (msg.payload !== "off" && msg.payload !== "hl18" && msg.payload !== "hl19") {
        return msg;
    } 

