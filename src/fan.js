let getMsg = msg.payload;
let ac = global.get('ac');

if (!ac) {
    ac = {
        power: "ON",
        mode: "fan_only",
        temperature: 24,
        fan: getMsg,
        swing: "on"
    }
} else {
    ac.fan = getMsg;
}

global.set('ac', ac);
return msg;
