let getMsg = parseFloat(msg.payload);
let ac = global.get('ac');

if (!ac) {
    ac = {
        power: "ON",
        mode: "fan_only",
        temperature: getMsg,
        fan: "medium",
        swing: "on"
    }
} else {
    ac.temperature = getMsg;
}

global.set('ac', ac);
return msg;
