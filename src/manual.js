let ac = global.get('ac');

if (ac.mode === "off") {
    msg.payload = "off";
} else if (ac.mode === "heat") {
    if (ac.temperature <= 18) {
        msg.payload = "hl18";
    } else if (ac.temperature <= 19) {
        msg.payload = "hl19";
    } else if (ac.temperature <= 20) {
        msg.payload = "hl20";
    } else if (ac.temperature <= 21) {
        msg.payload = "hl21";
        if (ac.fan === "medium" ) {
            msg.payload = "hm21";
        } else if (ac.fan === "high") {
            msg.payload = "hh21";
        }
    } else if (ac.temperature <= 22) {
        msg.payload = "hl22";
        if (ac.fan === "medium" ) {
            msg.payload = "hm22";
        } else if (ac.fan === "high") {
            msg.payload = "hh22";
        }
    } else if (ac.temperature <= 23) {
        msg.payload = "hl23";
        if (ac.fan === "medium" ) {
            msg.payload = "hm23";
        } else if (ac.fan === "high") {
            msg.payload = "hh23";
        }
    } else if (ac.temperature <= 24) {
        msg.payload = "hl24";
        if (ac.fan === "medium" ) {
            msg.payload = "hm24";
        } else if (ac.fan === "high") {
            msg.payload = "hh24";
        }
    }  else if (ac.temperature <= 25) {
        msg.payload = "hl25";
        if (ac.fan === "medium" ) {
            msg.payload = "hm25";
        } else if (ac.fan === "high") {
            msg.payload = "hh25";
        }
    }  else if (ac.temperature <= 26) {
        msg.payload = "hl26";
        if (ac.fan === "medium" ) {
            msg.payload = "hm26";
        } else if (ac.fan === "high") {
            msg.payload = "hh26";
        }
    } else {
        msg.payload = "hh26"
    }
} else if (ac.mode === "cool") {
    if (ac.temperature <= 23) {
        if (ac.swing === "off") {
            if (ac.fan === "low") {
                msg.payload = "cl23";
            } else if (ac.fan === "medium") {
                msg.payload = "cm23";
            } else {
                msg.payload = "ch23";
            }
        } else {
            if (ac.fan === "low") {
                msg.payload = "cls23";
            } else if (ac.fan === "medium") {
                msg.payload = "cms23";
            } else {
                msg.payload = "chs23";
            }
        }
    } else if (ac.temperature <= 24) {
        if (ac.swing === "off") {
            if (ac.fan === "low") {
                msg.payload = "cl24";
            } else if (ac.fan === "medium") {
                msg.payload = "cm24";
            } else {
                msg.payload = "ch24";
            }
        } else {
            if (ac.fan === "low") {
                msg.payload = "cls24";
            } else if (ac.fan === "medium") {
                msg.payload = "cms24";
            } else {
                msg.payload = "chs24";
            }
        }
    } else if (ac.temperature <= 25) {
        if (ac.swing === "off") {
            if (ac.fan === "low") {
                msg.payload = "cl25";
            } else if (ac.fan === "medium") {
                msg.payload = "cm25";
            } else {
                msg.payload = "ch25";
            }
        } else {
            if (ac.fan === "low") {
                msg.payload = "cls25";
            } else if (ac.fan === "medium") {
                msg.payload = "cms25";
            } else {
                msg.payload = "chs25";
            }
        }
    } else if (ac.temperature <= 26) {
        if (ac.swing === "off") {
            if (ac.fan === "low") {
                msg.payload = "cl26";
            } else if (ac.fan === "medium") {
                msg.payload = "cm26";
            } else {
                msg.payload = "ch26";
            }
        } else {
            if (ac.fan === "low") {
                msg.payload = "cls26";
            } else if (ac.fan === "medium") {
                msg.payload = "cms26";
            } else {
                msg.payload = "chs26";
            }
        }
    }
} else if (ac.mode === "fan_only") {
    if (ac.swing === "off") {
        if (ac.fan === "low") {
            msg.payload = "flu";
        } else if (ac.fan === "medium") {
            msg.payload = "fmm";
        } else {
            msg.payload = "fhm";
        }
    } else {
        if (ac.fan === "low") {
            msg.payload = "fls";
        } else if (ac.fan === "medium") {
            msg.payload = "fms";
        } else {
            msg.payload = "fhs";
        }
    }
} else if (ac.mode === "dry") {
    msg.payload = "dry"
}
return msg;
